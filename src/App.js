import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom'

function App() {
  return (
    <div className="App">
      
      <header className="App-header">
        <h1>Hello React</h1>
        <h3>Aplikasi React ini dibuat dengan create-react-app</h3>
        <p id="time"></p>
        <img src={logo} className="App-logo" alt="logo" />
       
      </header>
    </div>
  );
}


// function tick() {
//   const element = (
//     <div>
//       <h2>{new Date().toLocaleTimeString()}</h2>
//     </div>
//   );
//   ReactDOM.render(element, document.getElementById('time'));
// }

// setInterval(tick, 1000);

export default App;
