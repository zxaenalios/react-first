import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Greetings from './components/Greetings';
import Welcome from './components/Welcome';
import Selamat from './components/Selamat';
import Input from './components/Input';
import Button from './components/Button';
import logo from './logo.svg';

  const halo = React.createElement(
    'h1',
    {className: 'greeting'},
    'Halo, Dunia!'
  );

  const name = "Budi";
  const greeting = <h1>Halo, Selamat Pagi {name}</h1>
  const tahun_lahir = "1997";

  const ekspresi_js =
  <div>
    <h1 className="warna-judul"> Nama saya {name}</h1>
    <h2> Saya lahir tahun {tahun_lahir}</h2>
    <h3> Umur saya {2020-tahun_lahir}</h3>
  </div>

  ReactDOM.render(
    <React.StrictMode>
      {/* Function */}
      {/* <Greetings nama="Ali"/> */}
      {/* Class */}
      {/* <Welcome nama= "Imron"/> */}
      
      <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <Selamat nama="React"/>
      <Input placeholder="Username"/>
      <Input placeholder="Password"/>
      <Button/>
      </header>

    </React.StrictMode>
    ,document.getElementById('root')
  );




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
